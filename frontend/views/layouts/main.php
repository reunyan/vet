<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <div class="col-md-12 header-menu">
        <div class="col-md-7 p-0 float-left">
            <h3 class="text-white">ระบบโรงพยาบาลสัตว์ </h3>
        </div>
        <div class="coll-md-5 p-0 float-right">
            <h3><a href="<?= url::to('site/login')?>"> <i class="fas fa-user-circle text-white"></i></a></h3>
        </div>
    </div>

    <?php

    NavBar::begin([
        'brandLabel' => "",
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse  menu-color',
        ],
    ]);
    $menuItems = [
        ['label' => '<i class="fas fa-comment-medical"></i> ตรวจรักษา', 'url' => ['/site/index'],'icon'=> 'cog'],
        ['label' => '<i class="fas fa-comment-dollar"></i> จ่ายยา-คิดเงิน', 'url' => ['/site/about']],
        ['label' => '<i class="fas fa-heading"></i> เวชระเบียน', 'url' => ['/site/contact']],
        ['label' => '<i class="fas fa-warehouse"></i> คลังสินค้า', 'url' => ['/site/contact']],
        ['label' => '<i class="fas fa-book-open"></i> สมุดนัดหมาย', 'url' => ['/site/contact']],
        ['label' => '<i class="fas fa-temperature-high"></i> แล็ป', 'url' => ['/site/contact']],
        ['label' => 'ฝ่ายบุคคล', 'url' => ['/site/contact']],
        ['label' => '<i class="fas fa-user-cog"></i> ผู้จัดการ', 'url' => ['/site/contact']],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<?php /*
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
*/ ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
