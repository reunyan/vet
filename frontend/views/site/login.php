<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\AppAsset;

AppAsset::register($this);
$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="text-center body-login">
<?php $this->beginBody() ?>

        <div class="content-login">
            <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h1>เข้าสู่ระบบ</h1>
            <div class="row">
                <div class="col-lg-12">
                     <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->textInput()->input('username', ['placeholder' => "username"])->label(false) ?>

                        <?= $form->field($model, 'password')->passwordInput()->textInput()->input('password', ['placeholder' => "password"])->label(false) ?>


                        <div class="form-group">
                           <a href="/vet/frontend/web/" type="submit" class="btn btn-block btn-primary">Sign in</a>
                        </div>
                    <?php ActiveForm::end(); ?>
                    
                </div>
            </div>
        </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
