<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'medicalrecord' => [
            'class' => 'frontend\modules\medicalrecord\Module',
        ],
        'personnel' => [
            'class' => 'frontend\modules\personnel\Module',
        ],
        'diagnose' => [
            'class' => 'frontend\modules\diagnose\Module',
        ],
        'cashier' => [
            'class' => 'frontend\modules\cashier\Module',
        ],
        'appointment' => [
            'class' => 'frontend\modules\appointment\Module',
        ],
        'stock' => [
            'class' => 'frontend\modules\stock\Module',
        ],
        'lab' => [
            'class' => 'frontend\modules\lab\Module',
        ],
        'management' => [
            'class' => 'frontend\modules\management\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
